package javierdemarco.dontgetlost.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import javierdemarco.dontgetlost.R;
import javierdemarco.dontgetlost.dataTypes.HabitualPoint;
import javierdemarco.dontgetlost.dataTypes.HabitualPointList;

public class MakehabitualRouteAdapter extends RecyclerView.Adapter<MakehabitualRouteAdapter.MakeHabitualRouteListViewHolder>{

    private ArrayList<Spinner> list_Make_Route_Spinners;
    private HabitualPointList habitualPointList;

    public MakehabitualRouteAdapter(ArrayList<Spinner> list_Make_Route_Spinners, HabitualPointList habitualPointList) {
        this.list_Make_Route_Spinners = list_Make_Route_Spinners;
        this.habitualPointList = habitualPointList;
    }

    @NonNull
    @Override
    public MakehabitualRouteAdapter.MakeHabitualRouteListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = (View) LayoutInflater.from(parent.getContext()).inflate(R.layout.item_makehabitualroute, parent, false);

        return new MakehabitualRouteAdapter.MakeHabitualRouteListViewHolder(v, habitualPointList);
    }

    public void onBindViewHolder(@NonNull MakehabitualRouteAdapter.MakeHabitualRouteListViewHolder holder, int position) {
        //holder.spinner.set
        if (position % 2 == 1) {
            holder.view.setBackgroundResource(R.color.mapbox_blue);
        }
    }

    @Override
    public int getItemCount() {
        if(list_Make_Route_Spinners != null){
            return list_Make_Route_Spinners.size();
        }
        return 0;
    }

    public static class MakeHabitualRouteListViewHolder extends RecyclerView.ViewHolder {
        private final View view;
        public final Spinner spinner;


        public MakeHabitualRouteListViewHolder(View view, HabitualPointList habitualPointList) {
            super(view);
            this.view = view;
            ArrayList<String> habitualPointsString = new ArrayList<>();
            for (HabitualPoint h : habitualPointList.getHabitualPoints()) {
                habitualPointsString.add(h.getName());
            }
            // Create an ArrayAdapter using the string array and a default spinner layout
            ArrayAdapter<String> ad = new ArrayAdapter<String>(view.getContext(),
                    android.R.layout.simple_spinner_item,
                    habitualPointsString);
            // Specify the layout to use when the list of choices appears
            ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            this.spinner = view.findViewById(R.id.makehabitualroutespinner);
            this.spinner.setAdapter(ad);
        }

    }
}