package javierdemarco.dontgetlost.adapters;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

import javierdemarco.dontgetlost.R;
import javierdemarco.dontgetlost.activities.MapsActivity;
import javierdemarco.dontgetlost.dataTypes.RouteUser;
import javierdemarco.dontgetlost.dataTypes.RouteUserList;

public class FavUserListAdapter extends RecyclerView.Adapter<FavUserListAdapter.FavUserListViewHolder> {

    private RouteUserList favUserList;

    public FavUserListAdapter(RouteUserList favUserRoutes) {
        this.favUserList = favUserRoutes;
    }

    @NonNull
    @Override
    public FavUserListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = (View) LayoutInflater.from(parent.getContext()).inflate(R.layout.item_favuserroutes, parent, false);

        return new FavUserListAdapter.FavUserListViewHolder(v);
    }

    public void onBindViewHolder(@NonNull FavUserListViewHolder holder, int position) {
        holder.name.setText(favUserList.get(position).getName());
        holder.points = favUserList.get(position);
        if (position % 2 == 1) {
            holder.view.setBackgroundResource(R.color.mapbox_blue);
            holder.name.setTextColor(Color.WHITE);
        }
    }


    @Override
    public int getItemCount() {
        if(favUserList != null){
            return favUserList.size();
        }
        return 0;
    }

    public static class FavUserListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
            , View.OnLongClickListener {
        private final View view;
        public final TextView name;
        private RouteUser points;


        public FavUserListViewHolder(View view) {
            super(view);
            this.view = view;
            this.view.setOnClickListener(this);
            this.view.setOnLongClickListener(this);
            this.name = view.findViewById(R.id.routeName);

        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(v.getContext(), MapsActivity.class);
            Bundle b = new Bundle();
            b.putString("key", this.points.toString()); //Your id
            intent.putExtras(b); //Put your id to your next Intent
            v.getContext().startActivity(intent);
        }

        @Override
        public boolean onLongClick(View v) {
            String inName = this.name.getText().toString();
            AlertDialog.Builder errordiag = new AlertDialog.Builder(v.getContext());
            String m = view.getContext().getString(R.string.deleteRoute);
            errordiag.setMessage(m  + " " + inName)
                    .setPositiveButton("OK", new DeleteOnClickListener(v, inName))
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    }).show();
            return true;
        }
    }

    public static class DeleteOnClickListener implements DialogInterface.OnClickListener
    {
        private static final String FILENAME = "dontgetlost_user_routes";
        private String inName;
        private RouteUserList favUserRoutes;
        private View v;
        private File file;
        public DeleteOnClickListener(View v,String inName) {
            this.v = v;
            this.inName = inName;
        }

        @Override
        public void onClick(DialogInterface dialog, int which) {
            this.file = new File(v.getContext().getFilesDir(), FILENAME);
            this.readRoutes();
            this.favUserRoutes.removeByName(this.inName);
            this.writeRoutes();
        }

        private void readRoutes() {
            favUserRoutes = new RouteUserList();
            // read object from file
            try {
                FileReader filereader = new FileReader(this.file);
                BufferedReader br = new BufferedReader(filereader);
                String sCurrentLine;
                StringBuilder temp = new StringBuilder();
                while ((sCurrentLine = br.readLine()) != null) {
                    temp.append(sCurrentLine);
                }
                favUserRoutes = RouteUserList.processStringToArrayListRoutes(temp.toString());
                br.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void writeRoutes(){
            try {
                if (!file.exists()) {
                    file.createNewFile();
                }
                FileWriter fw = new FileWriter(file.getAbsoluteFile());
                BufferedWriter bw = new BufferedWriter(fw);
                bw.write(favUserRoutes.toString());
                bw.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
}
