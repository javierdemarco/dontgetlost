package javierdemarco.dontgetlost.adapters;

import android.content.DialogInterface;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

import javierdemarco.dontgetlost.R;
import javierdemarco.dontgetlost.dataTypes.HabitualPointList;



public class HabitualPointsListAdapter extends RecyclerView.Adapter<HabitualPointsListAdapter.HabitualPointsListViewHolder>{

        private HabitualPointList list_Habitual_Points;

        public HabitualPointsListAdapter(HabitualPointList list_Habitual_Points) {
            this.list_Habitual_Points = list_Habitual_Points;
        }

        @NonNull
        @Override
        public HabitualPointsListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = (View) LayoutInflater.from(parent.getContext()).inflate(R.layout.item_habitualpoint, parent, false);

            return new HabitualPointsListViewHolder(v);
        }

        public void onBindViewHolder(@NonNull HabitualPointsListViewHolder holder, int position) {
            holder.name.setText(list_Habitual_Points.get(position).getName());
            if (position % 2 == 1) {
                holder.view.setBackgroundResource(R.color.mapbox_blue);
                holder.name.setTextColor(Color.WHITE);
            }
        }

        @Override
        public int getItemCount() {
            if(list_Habitual_Points != null){
                return list_Habitual_Points.size();
            }
            return 0;
        }

        public static class HabitualPointsListViewHolder extends RecyclerView.ViewHolder
                implements View.OnLongClickListener {
            private final View view;
            public final TextView name;


            public HabitualPointsListViewHolder(View view) {
                super(view);
                this.view = view;
                this.view.setOnLongClickListener(this);
                this.name = view.findViewById(R.id.habitualpointname);

            }

            @Override
            public boolean onLongClick(View v) {
                String inName = this.name.getText().toString();
                AlertDialog.Builder errordiag = new AlertDialog.Builder(v.getContext());
                String habitualString = v.getContext().getString(R.string.deleteHabitualPoint);
                errordiag.setMessage(habitualString + " " + inName)
                        .setPositiveButton("OK", new HabitualPointsListAdapter.DeleteOnClickListener(v, inName))
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        }).show();
                return true;
            }
        }

        public static class DeleteOnClickListener implements DialogInterface.OnClickListener
        {
            private static final String FILENAME = "dontgetlost_user_habitual_points";
            private String inName;
            private HabitualPointList habitualpointslist;
            private View v;
            private File file;


            public DeleteOnClickListener(View v,String inName) {
                this.v = v;
                this.inName = inName;
            }

            @Override
            public void onClick(DialogInterface dialog, int which) {
                this.file = new File(v.getContext().getFilesDir(), FILENAME);
                this.readRoutes();
                this.habitualpointslist.removeByName(this.inName);
                this.writeRoutes();
            }

            private void readRoutes() {
                habitualpointslist = new HabitualPointList();
                // read object from file
                try {
                    FileReader filereader = new FileReader(this.file);
                    BufferedReader br = new BufferedReader(filereader);
                    String sCurrentLine;
                    StringBuilder temp = new StringBuilder();
                    while ((sCurrentLine = br.readLine()) != null) {
                        temp.append(sCurrentLine);
                    }
                    habitualpointslist = HabitualPointList.processStringToArrayListRoutes(temp.toString());
                    br.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            private void writeRoutes(){
                try {
                    if (!file.exists()) {
                        file.createNewFile();
                    }
                    FileWriter fw = new FileWriter(file.getAbsoluteFile());
                    BufferedWriter bw = new BufferedWriter(fw);
                    bw.write(habitualpointslist.toString());
                    bw.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
}
