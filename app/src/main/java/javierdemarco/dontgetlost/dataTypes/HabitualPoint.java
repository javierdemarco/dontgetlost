package javierdemarco.dontgetlost.dataTypes;

import com.mapbox.geojson.Point;

public class HabitualPoint {
    private String name;
    private Point point;

    public HabitualPoint(Point pInit, String nInit) {
        point = pInit;
        name = nInit;
    }

    public Point getPoint() {
        return point;
    }

    public String getName() {
        return name;
    }

    public void setName(String nSet) {
        name = nSet;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder(name + "=");
        s.append(point.longitude())
                .append(";")
                .append(point.latitude());
        return s.toString();
    }

    public static HabitualPoint processStringToPoint(String habitualName, String habitualPoint){
        Point p = Point.fromLngLat(
                Double.valueOf(habitualPoint.split(";")[0]),
                Double.valueOf(habitualPoint.split(";")[1])
        );
        return new HabitualPoint(p, habitualName);
    }
}
