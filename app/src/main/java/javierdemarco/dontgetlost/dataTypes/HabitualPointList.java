package javierdemarco.dontgetlost.dataTypes;

import java.util.ArrayList;

public class HabitualPointList {

    private ArrayList<HabitualPoint> habitualPoints;

    public HabitualPointList(){
        this.habitualPoints = new ArrayList<>();
    }

    public ArrayList<HabitualPoint> getHabitualPoints() {
        return habitualPoints;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();

        for(HabitualPoint r : habitualPoints){
            if (!r.equals(habitualPoints.get(habitualPoints.size()-1)) || habitualPoints.size() == 1){
                s.append(r).append(",");
            }else {
                s.append(r);
            }
        }
        return s.toString();
    }

    public HabitualPoint get(int position) {
        return habitualPoints.get(position);
    }

    public int size() {
        return habitualPoints.size();
    }

    public void add(HabitualPoint habitualPoints) {
        this.habitualPoints.add(habitualPoints);
    }

    public void removeByName(String inName){
        for(HabitualPoint r : this.habitualPoints){
            if(r.getName().equals(inName)){
                this.habitualPoints.remove(r);
            }
        }
    }

    public static HabitualPointList processStringToArrayListRoutes(String inString){
        String name;
        String pointString;
        HabitualPointList habitualPoints = new HabitualPointList();
        String[] habitualPointsStrings = inString.split(",");
        for (String habitualpoint : habitualPointsStrings){
            name = habitualpoint.split("=")[0];
            pointString = habitualpoint.split("=")[1];
            habitualPoints.add(HabitualPoint.processStringToPoint(name, pointString));
        }
        return habitualPoints;
    }

    public HabitualPoint getByString(String inString) {
        for (HabitualPoint i: this.habitualPoints){
            if (i.getName().equals(inString)){
                return i;
            }
        }
        return null;
    }
}
