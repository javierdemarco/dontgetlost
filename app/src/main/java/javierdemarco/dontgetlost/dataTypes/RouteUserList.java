package javierdemarco.dontgetlost.dataTypes;

import java.io.Serializable;
import java.util.ArrayList;

public class RouteUserList implements Serializable {

    private ArrayList<RouteUser> routes;

    public RouteUserList(ArrayList<RouteUser> routes) {
        this.routes = routes;
    }

    public RouteUserList (RouteUser r){
        this.routes = new ArrayList<RouteUser>();
        this.routes.add(r);
    }

    public RouteUserList(){
        this.routes = new ArrayList<RouteUser>();
    }

    public ArrayList<RouteUser> getRoutes() {
        return routes;
    }

    public void setRoutes(ArrayList<RouteUser> routes) {
        this.routes = routes;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();

        for(RouteUser r : routes){
            if (!r.equals(routes.get(routes.size()-1)) || routes.size() == 1){
                s.append(r).append(",");
            }else {
                s.append(r);
            }
        }
        return s.toString();
    }

    public RouteUser get(int position) {
        return routes.get(position);
    }

    public int size() {
        return routes.size();
    }

    public void add(RouteUser route) {
        this.routes.add(route);
    }

    public void removeByName(String inName){
        for(RouteUser r : this.routes){
            if(r.getName().equals(inName)){
                this.routes.remove(r);
            }
        }
    }

    public static RouteUserList processStringToArrayListRoutes(String inString){
        String name;
        String[] pointsString;
        RouteUserList favUserRoutes = new RouteUserList();
        String[] routesStrings = inString.split(",");
        for (String route : routesStrings){
            name = route.split("=")[0];
            pointsString = route.split("=")[1].split("/");
            favUserRoutes.add(RouteUser.processStringToRoute(name, pointsString));
        }
        return favUserRoutes;
    }
}
