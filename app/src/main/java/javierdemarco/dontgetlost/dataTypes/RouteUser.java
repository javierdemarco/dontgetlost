package javierdemarco.dontgetlost.dataTypes;

import com.mapbox.geojson.Point;

import java.io.Serializable;

public class RouteUser implements Serializable {

    private String name;
    private Point[] points;

    public RouteUser(Point[] pInit, String nInit) {

        points = new Point[pInit.length];
        System.arraycopy(pInit, 0, points, 0, pInit.length);
        name = nInit;
    }

    public RouteUser(){
        name = "";
        points = new Point[0];
    }

    public void addPoint(Point point){
        Point[] aux = points.clone();
        points = new Point[points.length+1];
        System.arraycopy(aux, 0, points, 0, aux.length);
        points[points.length-1] = point;
    }

    public Point[] getPoints() {
        return points;
    }

    public String getName() {
        return name;
    }

    public void setName(String nSet) {
        name = nSet;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder(name + "=");
        int i = 0;
        for(Point p : points){
            if(i == points.length-1){
                s.append(p.longitude())
                        .append(";")
                        .append(p.latitude());
            }else {
                s.append(p.longitude())
                        .append(";")
                        .append(p.latitude())
                        .append("/");
            }
            i++;
        }
        return s.toString();
    }

    public static RouteUser processStringToRoute(String routeName, String[] routePoints){
        Point[] points = new Point[1];
        RouteUser routeUser = new RouteUser();
        int i = 0;
        for(String point : routePoints){
            if (i == 0){
                points[0] =
                        Point.fromLngLat(
                                Double.valueOf(point.split(";")[0]),
                                Double.valueOf(point.split(";")[1])
                        );
                routeUser = new RouteUser(points, routeName);
            }
            else {
                routeUser.addPoint(
                        Point.fromLngLat(
                                Double.valueOf(point.split(";")[0]),
                                Double.valueOf(point.split(";")[1])
                        )
                );
            }
            i++;
        }
        return routeUser;
    }

    public static Point[] processStringToPoints(String[] routePoints){
        Point[] points = new Point[1];
        RouteUser routeUser = new RouteUser();
        int i = 0;
        for(String point : routePoints){
            if (i == 0){
                points[0] =
                        Point.fromLngLat(
                                Double.valueOf(point.split(";")[0]),
                                Double.valueOf(point.split(";")[1])
                        );
                routeUser = new RouteUser(points, "nothing");
            }
            else {
                routeUser.addPoint(
                        Point.fromLngLat(
                                Double.valueOf(point.split(";")[0]),
                                Double.valueOf(point.split(";")[1])
                        )
                );
            }
            i++;
        }
        return routeUser.getPoints();
    }
}
