package javierdemarco.dontgetlost.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

import javierdemarco.dontgetlost.R;
import javierdemarco.dontgetlost.adapters.MakehabitualRouteAdapter;
import javierdemarco.dontgetlost.dataTypes.HabitualPoint;
import javierdemarco.dontgetlost.dataTypes.HabitualPointList;
import javierdemarco.dontgetlost.dataTypes.RouteUser;

public class ListMakeHabitualRoute extends AppCompatActivity {
    private RecyclerView makeRouteView;
    private RecyclerView.Adapter adapter;
    private ArrayList<Spinner> makeRouteSpinners;
    private HabitualPointList habitualPointList;
    private RecyclerView.LayoutManager mLayoutManager;
    private static final String FILENAME_HABITUAL_POINTS = "dontgetlost_user_habitual_points";
    private static final String FILENAME_ROUTES = "dontgetlost_user_routes";
    private File file;
    private File fileRoutes;
    private Button buttonAddPoint;
    private Button buttonMakeRoute;
    private HabitualPointList habitualRoute;
    private RouteUser userHabitualRoute;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listmake_habitual_route);

        file = new File(getApplicationContext().getFilesDir(), FILENAME_HABITUAL_POINTS);
        fileRoutes = new File(getApplicationContext().getFilesDir(), FILENAME_ROUTES);

        buttonAddPoint = findViewById(R.id.buttonRefresh);
        buttonAddPoint.setOnClickListener(this::setButtonAddPoint);

        buttonMakeRoute = findViewById(R.id.buttonMakeRoute);
        buttonMakeRoute.setOnClickListener(this::setButtonMakeRoute);

        makeRouteSpinners = new ArrayList<>();
        habitualRoute = new HabitualPointList();
        userHabitualRoute = new RouteUser();
        initHabitualPoints();

        this.makeRouteView = findViewById(R.id.make_habitual_route_view);
        mLayoutManager = new LinearLayoutManager(this);
        this.makeRouteView.setLayoutManager(mLayoutManager);

        Spinner ss = (Spinner)findViewById(R.id.makehabitualroutespinner);
        makeRouteSpinners.add(ss);
        adapter = new MakehabitualRouteAdapter(makeRouteSpinners, habitualPointList);
        this.makeRouteView.setAdapter(adapter);
    }

    private void initHabitualPoints() {
        habitualPointList = new HabitualPointList();
        // read object from file
        try {
            FileReader filereader = new FileReader(this.file);
            BufferedReader br = new BufferedReader(filereader);
            String sCurrentLine;
            StringBuilder temp = new StringBuilder();
            while ((sCurrentLine = br.readLine()) != null) {
                temp.append(sCurrentLine);
            }
            habitualPointList = HabitualPointList.processStringToArrayListRoutes(temp.toString());
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        adapter = new MakehabitualRouteAdapter(makeRouteSpinners, habitualPointList);
        this.makeRouteView.setAdapter(adapter);
    }

    public void setButtonAddPoint(View v) {
        refreshList();
    }

    public void setButtonMakeRoute(View view) {
        ArrayList<String> names = new ArrayList<>();
        int i = 0;
        try {
            while (true){
                names.add(((Spinner)this.makeRouteView.getLayoutManager().findViewByPosition(i)
                        .findViewById(R.id.makehabitualroutespinner)).getSelectedItem().toString());
                i++;
            }
        } catch (Exception e) {
        }
        if(names.size() > 1) {
            for (String s : names) {
                this.habitualRoute.add(this.habitualPointList.getByString(s));
            }
            StringBuilder userRouteName = new StringBuilder();
            i = 0;
            for (HabitualPoint p : this.habitualRoute.getHabitualPoints()) {
                userHabitualRoute.addPoint(p.getPoint());
                userRouteName.append(p.getName());
                if(i != this.habitualPointList.size()) {
                    userRouteName.append("-");
                }
                i++;
            }
            userHabitualRoute.setName(userRouteName.toString());
            writeRoute();
        }else{
            Toast.makeText(this, R.string.cant_save_habitual_route, Toast.LENGTH_LONG).show();
        }
    }

    public void writeRoute(){
        // write object to file
        try {
            StringBuilder temp = new StringBuilder();
            if (!fileRoutes.exists()) {
                fileRoutes.createNewFile();
            } else {
                FileReader filereader = new FileReader(fileRoutes);
                BufferedReader br = new BufferedReader(filereader);
                String sCurrentLine;
                while ((sCurrentLine = br.readLine()) != null) {
                    temp.append(sCurrentLine);
                }
            }
            if(temp.toString().contains(this.userHabitualRoute.getName())){
                AlertDialog.Builder errordiag = new AlertDialog.Builder(this);
                errordiag.setMessage(R.string.errorSave)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        }).show();
            }else {
                temp.append(this.userHabitualRoute.toString() + ",");
                FileWriter fw = new FileWriter(fileRoutes.getAbsoluteFile());
                BufferedWriter bw = new BufferedWriter(fw);
                bw.write(temp.toString());
                bw.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Toast.makeText(this, R.string.save_route, Toast.LENGTH_LONG).show();
    }

    public void refreshList() {
        Spinner ss = (Spinner)findViewById(R.id.makehabitualroutespinner);
        makeRouteSpinners.add(ss);

        adapter = new MakehabitualRouteAdapter(makeRouteSpinners, habitualPointList);
        this.makeRouteView.setAdapter(adapter);
    }
}

