package javierdemarco.dontgetlost.activities;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import javierdemarco.dontgetlost.R;
import javierdemarco.dontgetlost.adapters.HabitualPointsListAdapter;
import javierdemarco.dontgetlost.dataTypes.HabitualPointList;

public class ListHabitualPointsActivity extends AppCompatActivity {
    private RecyclerView habitualPointsView;
    private RecyclerView.Adapter adapter;
    private static final String FILENAME_HABITUAL_POINTS = "dontgetlost_user_habitual_points";
    private HabitualPointList habitualPointsList;
    private File file;
    private RecyclerView.LayoutManager mLayoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listhabitual_points);

        file = new File(getApplicationContext().getFilesDir(), FILENAME_HABITUAL_POINTS);

        readRoutes();

        this.habitualPointsView = findViewById(R.id.habitual_points_recycler_view);
        mLayoutManager = new LinearLayoutManager(this);
        this.habitualPointsView.setLayoutManager(mLayoutManager);

        adapter = new HabitualPointsListAdapter(habitualPointsList);
        this.habitualPointsView.setAdapter(adapter);

    }

    @Override
    protected void onResume() {
        super.onResume();
        readRoutes();
        adapter = new HabitualPointsListAdapter(habitualPointsList);
        this.habitualPointsView.setAdapter(adapter);
    }

    public void readRoutes() {
        habitualPointsList = new HabitualPointList();
        // read object from file
        try {
            FileReader filereader = new FileReader(this.file);
            BufferedReader br = new BufferedReader(filereader);
            String sCurrentLine;
            StringBuilder temp = new StringBuilder();
            while ((sCurrentLine = br.readLine()) != null) {
                temp.append(sCurrentLine);
            }
            habitualPointsList = HabitualPointList.processStringToArrayListRoutes(temp.toString());
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void refreshList(View view) {
        readRoutes();
        adapter = new HabitualPointsListAdapter(habitualPointsList);
        this.habitualPointsView.setAdapter(adapter);
    }
}
