package javierdemarco.dontgetlost.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import javierdemarco.dontgetlost.R;
import javierdemarco.dontgetlost.adapters.FavUserListAdapter;
import javierdemarco.dontgetlost.dataTypes.RouteUserList;

public class ListActivity extends AppCompatActivity{
    private RecyclerView favUserView;
    private RecyclerView.Adapter adapter;
    private static final String FILENAME_ROUTES = "dontgetlost_user_routes";
    private RouteUserList favUserRoutes;
    private File file;
    private final int REQUEST_LOCATION = 99;
    private RecyclerView.LayoutManager mLayoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        file = new File(getApplicationContext().getFilesDir(), FILENAME_ROUTES);

        readRoutes();

        locationPermission();

        this.favUserView = findViewById(R.id.favUserListView);
        mLayoutManager = new LinearLayoutManager(this);
        this.favUserView.setLayoutManager(mLayoutManager);

        adapter = new FavUserListAdapter(favUserRoutes);
        this.favUserView.setAdapter(adapter);

    }

    @Override
    protected void onResume() {
        super.onResume();
        readRoutes();
        adapter = new FavUserListAdapter(favUserRoutes);
        this.favUserView.setAdapter(adapter);
    }

    public void readRoutes() {
        favUserRoutes = new RouteUserList();
        // read object from file
        try {
            FileReader filereader = new FileReader(this.file);
            BufferedReader br = new BufferedReader(filereader);
            String sCurrentLine;
            StringBuilder temp = new StringBuilder();
            while ((sCurrentLine = br.readLine()) != null) {
                temp.append(sCurrentLine);
            }
            favUserRoutes = RouteUserList.processStringToArrayListRoutes(temp.toString());
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void goToMaps(View view) {
        startActivity(new Intent(ListActivity.this, MapsActivity.class));
    }

    private  void locationPermission(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Check Permissions Now
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION);
        }
    }

    public void refreshList(View view) {
        readRoutes();
        adapter = new FavUserListAdapter(favUserRoutes);
        this.favUserView.setAdapter(adapter);
    }
}