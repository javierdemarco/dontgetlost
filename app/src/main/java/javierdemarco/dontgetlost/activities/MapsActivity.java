package javierdemarco.dontgetlost.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.mapbox.android.core.BuildConfig;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.api.directions.v5.DirectionsCriteria;
import com.mapbox.api.directions.v5.models.DirectionsResponse;
import com.mapbox.api.directions.v5.models.DirectionsRoute;
import com.mapbox.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.plugins.localization.LocalizationPlugin;
import com.mapbox.mapboxsdk.plugins.places.autocomplete.PlaceAutocomplete;
import com.mapbox.mapboxsdk.plugins.places.autocomplete.model.PlaceOptions;
import com.mapbox.mapboxsdk.style.layers.CircleLayer;
import com.mapbox.mapboxsdk.style.layers.LineLayer;
import com.mapbox.mapboxsdk.style.layers.Property;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.mapbox.services.android.navigation.ui.v5.NavigationLauncher;
import com.mapbox.services.android.navigation.ui.v5.NavigationLauncherOptions;
import com.mapbox.services.android.navigation.ui.v5.route.NavigationMapRoute;
import com.mapbox.services.android.navigation.v5.navigation.NavigationRoute;

import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.List;
import java.util.Objects;

import javierdemarco.dontgetlost.R;
import javierdemarco.dontgetlost.dataTypes.HabitualPoint;
import javierdemarco.dontgetlost.dataTypes.HabitualPointList;
import javierdemarco.dontgetlost.dataTypes.RouteUser;
import javierdemarco.dontgetlost.dataTypes.RouteUserList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.mapbox.mapboxsdk.style.expressions.Expression.eq;
import static com.mapbox.mapboxsdk.style.expressions.Expression.literal;
import static com.mapbox.mapboxsdk.style.layers.Property.CIRCLE_PITCH_ALIGNMENT_MAP;
import static com.mapbox.mapboxsdk.style.layers.Property.CIRCLE_PITCH_ALIGNMENT_VIEWPORT;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.circleColor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.circlePitchAlignment;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.circleRadius;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineCap;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineColor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineWidth;

/**
 * Use the Mapbox Directions API to request and retrieve a Directions route. Show the route line and
 * place a circle where each of the route's step maneuver locations are.
 */
public class MapsActivity extends AppCompatActivity
        implements OnMapReadyCallback, MapboxMap.OnMapClickListener, PermissionsListener {

    private static final String CIRCLE_GEOJSON_SOURCE_ID = "CIRCLE_GEOJSON_SOURCE_ID";
    private static final String LINE_GEOJSON_SOURCE_ID = "LINE_GEOJSON_SOURCE_ID";
    private static final String STEPS_CIRCLE_LAYER_ID = "steps-circle-layer";
    private static final String STEPS_BACKGROUND_CIRCLE_LAYER_ID = "steps-background-circle-layer";
    private static final String DIRECTIONS_ROUTE_LINE_LAYER_ID = "directions-line-layer";
    private static final String SETTLEMENT_LABEL_LAYER_ID = "settlement-label";
    private static final int REQUEST_CODE_AUTOCOMPLETE = 1;
    private static final int REQUEST_CODE_ADD_HABITUAL_POINT = 2;
    private static final int MAX_WAYPOINTS = 1;
    private static final String FILENAME_ROUTES = "dontgetlost_user_routes";
    private static final String FILENAME_HABITUAL_POINTS = "dontgetlost_user_habitual_points";

    // Adjust the following private static final variables to style this example's UI
    private static final String LINE_COLOR = "#4169E1";
    private static final float LINE_WIDTH = 8f;
    private static final float CIRCLE_RADIUS = 8f;
    private static final float RADIUS_DIFFERENCE_BETWEEN_WAYPOINT_CIRCLES_AND_BACKGROUND_CIRCLES = 3f;
    private static final int CIRCLE_COLOR = Color.WHITE;
    private static final int BACKGROUND_CIRCLE_COLOR = Color.parseColor(LINE_COLOR);
    private static final boolean ALIGN_CIRCLES_WITH_MAP = true;

    private MapView mapView;
    private MapboxMap mapboxMap;
    private DirectionsRoute currentRoute;
    private DirectionsRoute userRoute;
    private Button buttonStartNavigation;
    private Button buttonSaveRoute;
    private Button buttonUndo;
    private LocationComponent locationComponent;
    private Point destination;
    private Point origin;
    private PermissionsManager permissionsManager;
    private NavigationMapRoute navigationMapRoute;
    private Point[] waypoints;
    private String favRouteName;
    private RouteUser favRoute;
    private Point userLocation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

        // Mapbox access token is configured here. This needs to be called either in your application
        // object or in the same activity which contains the mapview.
        Mapbox.getInstance(this, getString(R.string.mapbox_access_token));

        // This contains the MapView in XML and needs to be called after the access token is configured.
        setContentView(R.layout.activity_maps);

        mapView = findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        waypoints = new Point[MAX_WAYPOINTS];
    }

    private void getExtrasInBundle() {
        Bundle b = getIntent().getExtras();
        Point[] r;
        String extra;
        String[] extraPoints;
        if (b != null) {
            extra = b.getString("key");
            assert extra != null;
            extraPoints = extra.split("=")[1].split("/");
            r = RouteUser.processStringToPoints(extraPoints);
            this.origin = r[0];
            if(r.length - 2 == 0){
                this.waypoints = new Point[1];
            }else {
                waypoints = new Point[r.length - 2];
            }
            System.arraycopy(r, 1, waypoints, 0, r.length - 2);
            destination = r[r.length - 1];
            if(this.waypoints[0] == null){
                this.getRoute(origin, destination);
                this.waypoints = new  Point[1];
                this.waypoints[0] = destination;
            }else {
                addRoute(destination);
            }
        }
    }

    @Override
    public void onMapReady(@NonNull MapboxMap mapboxMap) {
        this.mapboxMap = mapboxMap;
        mapboxMap.setStyle(new Style.Builder().fromUri(Style.OUTDOORS)
                .withSource(new GeoJsonSource(CIRCLE_GEOJSON_SOURCE_ID))
                .withSource(new GeoJsonSource(LINE_GEOJSON_SOURCE_ID)), style -> {
            enableLocationComponent(style);
            initLineLayerForDirectionsRoute(style);
            initStepManeuverCircleLayer(style);
            initStepManeuverBackgroundCircleLayer(style);
            initSearchFab();
            mapboxMap.addOnMapClickListener(MapsActivity.this);
            buttonStartNavigation = findViewById(R.id.startButton);
            buttonStartNavigation.setOnClickListener(this::setButtonStartNavigationClickFunction);
            buttonSaveRoute = findViewById(R.id.saveRoute);
            buttonSaveRoute.setOnClickListener(this::setButtonSaveRouteClickFunction);
            buttonUndo = findViewById(R.id.undo);
            buttonUndo.setOnClickListener(this::setButtonUndoClickFunction);
            LocalizationPlugin localizationPlugin = new LocalizationPlugin(mapView, mapboxMap, style);
            try {
                localizationPlugin.matchMapLanguageWithDeviceDefault();
            } catch (RuntimeException exception) {
                Timber.d(exception.toString());
            }
        });
        this.getExtrasInBundle();

    }

    public void setButtonStartNavigationClickFunction(View v) {
        if(this.origin != null && this.destination != null) {
            boolean simulateRoute = true;
            NavigationLauncherOptions options = NavigationLauncherOptions.builder()
                    .directionsRoute(userRoute)
                    .shouldSimulateRoute(simulateRoute)
                    .build();
            // Call this method with Context from within an Activity
            NavigationLauncher.startNavigation(MapsActivity.this, options);
        }else{
            Toast.makeText(this, R.string.cant_start_navigation, Toast.LENGTH_LONG).show();
        }
    }

    public void setButtonSaveRouteClickFunction(View v) {
        if(this.origin != null && this.destination != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
            builder.setTitle(R.string.tile_save_route_dialog);

            // Set up the input
            final EditText input = new EditText(v.getContext());
            // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
            input.setInputType(InputType.TYPE_CLASS_TEXT);
            builder.setView(input);

            // Set up the buttons
            builder.setPositiveButton("OK", (dialog, which) -> {
                Point[] route = new Point[waypoints.length + 1];
                favRouteName = input.getText().toString();
                route[0] = origin;
                System.arraycopy(waypoints, 0, route, 1, waypoints.length);
                favRoute = new RouteUser(route, favRouteName);
                RouteUserList favUserRoutes = new RouteUserList(favRoute);
                // write object to file
                try {
                    File file = new File(getApplicationContext().getFilesDir(), FILENAME_ROUTES);
                StringBuilder temp = new StringBuilder();
                    if (!file.exists()) {
                        file.createNewFile();
                    } else {
                        FileReader filereader = new FileReader(file);
                        BufferedReader br = new BufferedReader(filereader);
                        String sCurrentLine;
                        while ((sCurrentLine = br.readLine()) != null) {
                            temp.append(sCurrentLine);
                        }
                    }
                    if(temp.toString().contains(favRouteName)){
                        AlertDialog.Builder errordiag = new AlertDialog.Builder(this);
                        errordiag.setMessage(R.string.errorSave)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                    }
                                }).show();
                    }else {
                        temp.append(favUserRoutes.toString());
                        FileWriter fw = new FileWriter(file.getAbsoluteFile());
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write(temp.toString());
                        bw.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());
            builder.show();
        }else{
            Toast.makeText(this, R.string.cant_save_route, Toast.LENGTH_LONG).show();
        }
    }

    public void setButtonUndoClickFunction(View v) {
        if(this.origin != null) {
            this.undoPoint();
        }else{
            Toast.makeText(this, R.string.cant_undo_points, Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Create and style a LineLayer that will draw the Mapbox Directions API route line.
     *
     * @param loadedMapStyle the map's {@link Style} object
     */
    private void initLineLayerForDirectionsRoute(@NonNull Style loadedMapStyle) {
        LineLayer directionsRouteLineLayer = new LineLayer(DIRECTIONS_ROUTE_LINE_LAYER_ID, LINE_GEOJSON_SOURCE_ID);
        directionsRouteLineLayer.setProperties(
                lineColor(Color.parseColor(LINE_COLOR)),
                lineCap(Property.LINE_CAP_ROUND),
                lineWidth(LINE_WIDTH)
        );

        directionsRouteLineLayer.setFilter(eq(literal("$type"), literal("LineString")));

        // Add the layer below the "settlement-label" layer (city name labels, etc.)
        if (loadedMapStyle.getLayer(SETTLEMENT_LABEL_LAYER_ID) != null) {
            loadedMapStyle.addLayerBelow(directionsRouteLineLayer, SETTLEMENT_LABEL_LAYER_ID);
        } else {
            loadedMapStyle.addLayer(directionsRouteLineLayer);
        }

    }

    /**
     * Create and style a CircleLayer that will place circles for each of the Mapbox Directions API
     * route's step maneuver locations.
     *
     * @param loadedMapStyle the map's {@link Style} object
     */
    private void initStepManeuverCircleLayer(@NonNull Style loadedMapStyle) {
        CircleLayer individualCirclesLayer = new CircleLayer(STEPS_CIRCLE_LAYER_ID, CIRCLE_GEOJSON_SOURCE_ID);
        individualCirclesLayer.setProperties(
                circleColor(CIRCLE_COLOR),
                circlePitchAlignment(ALIGN_CIRCLES_WITH_MAP ? CIRCLE_PITCH_ALIGNMENT_MAP : CIRCLE_PITCH_ALIGNMENT_VIEWPORT),
                circleRadius(CIRCLE_RADIUS));
        loadedMapStyle.addLayer(individualCirclesLayer);
    }

    /**
     * Create and style a CircleLayer that will place circles beneath the STEPS_CIRCLE_LAYER_ID layer
     * for each of the Mapbox Directions API route's step maneuver locations.
     *
     * @param loadedMapStyle the map's {@link Style} object
     */
    private void initStepManeuverBackgroundCircleLayer(@NonNull Style loadedMapStyle) {
        CircleLayer individualCirclesLayer = new CircleLayer(STEPS_BACKGROUND_CIRCLE_LAYER_ID, CIRCLE_GEOJSON_SOURCE_ID);
        individualCirclesLayer.setProperties(
                circleColor(BACKGROUND_CIRCLE_COLOR),
                circlePitchAlignment(ALIGN_CIRCLES_WITH_MAP ? CIRCLE_PITCH_ALIGNMENT_MAP : CIRCLE_PITCH_ALIGNMENT_VIEWPORT),
                circleRadius(CIRCLE_RADIUS + RADIUS_DIFFERENCE_BETWEEN_WAYPOINT_CIRCLES_AND_BACKGROUND_CIRCLES));
        loadedMapStyle.addLayerBelow(individualCirclesLayer, STEPS_CIRCLE_LAYER_ID);
    }

    @SuppressWarnings({"MissingPermission"})
    @Override
    public boolean onMapClick(@NonNull LatLng point) {
        if (this.origin == null) {
            this.origin = Point.fromLngLat(point.getLongitude(), point.getLatitude());
            getRoute(origin, origin);
        } else {
            destination = Point.fromLngLat(point.getLongitude(), point.getLatitude());
            manageRoutes(destination);
        }
        return true;
    }

    private void manageRoutes(Point destination) {
        if(this.waypoints[0] == null){
            this.getRoute(origin, destination);
            this.waypoints = new  Point[1];
            this.waypoints[0] = destination;
        }else {
            addRoute(destination);
        }
        userLocation = Point.fromLngLat(locationComponent.getLastKnownLocation().getLongitude(),
                locationComponent.getLastKnownLocation().getLatitude());
        this.getRouteUser(destination);
    }

    /**
     * Make a request to the Mapbox Directions API. Once successful, pass the route to the
     * route layer.
     *
     * @param origin      the starting point of the route
     * @param destination the desired finish point of the route
     */
    private void getRoute(Point origin, Point destination) {
        assert Mapbox.getAccessToken() != null;
        NavigationRoute.builder(this)
                .accessToken(Mapbox.getAccessToken())
                .origin(origin)
                .destination(destination)
                .profile(DirectionsCriteria.PROFILE_WALKING)
                .build()
                .getRoute(new Callback<DirectionsResponse>() {
                    @Override
                    public void onResponse(@NotNull Call<DirectionsResponse> call, @NotNull Response<DirectionsResponse> response) {
                        // You can get the generic HTTP info about the response
                        Timber.d("Response code: %s", response.code());
                        if (response.body() == null) {
                            Timber.e("No routes found, make sure you set the right user and access token.");
                            return;
                        } else if (response.body().routes().size() < 1) {
                            Timber.e("No routes found");
                            return;
                        }
                        currentRoute = response.body().routes().get(0);
                        // Draw the route on the map
                        if (navigationMapRoute != null) {
                            navigationMapRoute.removeRoute();
                        } else {
                            navigationMapRoute = new NavigationMapRoute(null, mapView, mapboxMap, R.style.NavigationMapRoute);
                        }
                        navigationMapRoute.addRoute(currentRoute);
                    }

                    @Override
                    public void onFailure(@NotNull Call<DirectionsResponse> call, @NotNull Throwable throwable) {
                        Timber.e("Error: %s", throwable.getMessage());
                    }
                });
    }

    private void getRouteUser(Point destination) {
        assert Mapbox.getAccessToken() != null;
        NavigationRoute.Builder builder = NavigationRoute.builder(this)
                .accessToken(Mapbox.getAccessToken())
                .origin(userLocation)
                .destination(destination)
                .profile(DirectionsCriteria.PROFILE_WALKING);
        if(this.waypoints[0] != null) {
            Point[] resultwaypoints = new Point[waypoints.length + 1];
            System.arraycopy(waypoints, 0, resultwaypoints, 1, waypoints.length);
            Point originclone = origin;
            resultwaypoints[0] = originclone;
            for (Point waypoint : resultwaypoints) {
                builder.addWaypoint(waypoint);
            }
        }
        builder.build().getRoute(new Callback<DirectionsResponse>() {
            @Override
            public void onResponse(@NotNull Call<DirectionsResponse> call, @NotNull Response<DirectionsResponse> response) {
                // You can get the generic HTTP info about the response
                Timber.d("Response code: %s", response.code());
                if (response.body() == null) {
                    Timber.e("No routes found, make sure you set the right user and access token.");
                    return;
                } else if (response.body().routes().size() < 1) {
                    Timber.e("No routes found");
                    return;
                }
                userRoute = response.body().routes().get(0);
            }

            @Override
            public void onFailure(@NotNull Call<DirectionsResponse> call, @NotNull Throwable throwable) {
                Timber.e("Error: %s", throwable.getMessage());
            }
        });

    }

    public void addRoute(Point destination) {

        assert Mapbox.getAccessToken() != null;
        NavigationRoute.Builder builder = NavigationRoute.builder(this)
                .accessToken(Mapbox.getAccessToken())
                .origin(origin)
                .destination(destination)
                .profile(DirectionsCriteria.PROFILE_WALKING);

        if(this.waypoints[0] != null) {
            if (this.waypoints[0].coordinates() != this.destination.coordinates()) {
                for (Point waypoint : waypoints) {
                    if (waypoint.coordinates() != this.destination.coordinates()) {
                        builder.addWaypoint(waypoint);
                    }
                }
            }
        }

        builder.build().getRoute(new Callback<DirectionsResponse>() {
            @Override
            public void onResponse(@NotNull Call<DirectionsResponse> call, @NotNull Response<DirectionsResponse> response) {
                // You can get the generic HTTP info about the response
                Timber.d("Response code: %s", response.code());
                if (response.body() == null) {
                    Timber.e("No routes found, make sure you set the right user and access token.");
                    return;
                } else if (response.body().routes().size() < 1) {
                    Timber.e("No routes found");
                    return;
                }
                currentRoute = response.body().routes().get(0);
                // Draw the route on the map
                if (navigationMapRoute != null) {
                    navigationMapRoute.removeRoute();
                } else {
                    navigationMapRoute = new NavigationMapRoute(null, mapView, mapboxMap, R.style.NavigationMapRoute);
                }
                navigationMapRoute.addRoute(currentRoute);
            }

            @Override
            public void onFailure(@NotNull Call<DirectionsResponse> call, @NotNull Throwable throwable) {
                Timber.e("Error: %s", throwable.getMessage());
            }
        });
        if(this.waypoints[0] != null) {
            if (this.waypoints[0].coordinates() != this.destination.coordinates()) {
                Point[] aux = waypoints.clone();
                waypoints = new Point[waypoints.length + 1];
                System.arraycopy(aux, 0, waypoints, 0, aux.length);
                waypoints[waypoints.length - 1] = destination;
            }
        }
    }

    private void initSearchFab() {
        this.userLocation = Point.fromLngLat(locationComponent.getLastKnownLocation().getLongitude(),
                locationComponent.getLastKnownLocation().getLatitude());
        findViewById(R.id.fav_location_search).setOnClickListener(view -> {
            Intent intent = new PlaceAutocomplete.IntentBuilder()
                    .accessToken(Mapbox.getAccessToken() != null ? Mapbox.getAccessToken() : getString(R.string.mapbox_access_token))
                    .placeOptions(PlaceOptions.builder()
                            .backgroundColor(Color.parseColor("#EEEEEE"))
                            .limit(10)
                            .proximity(this.userLocation)
                            .build(PlaceOptions.MODE_CARDS))
                    .build(MapsActivity.this);
            startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE);
        });
    }

    private void undoPoint(){
        if(this.destination == null){
            this.origin = null;
            this.currentRoute = null;
            this.userRoute = null;
            this.navigationMapRoute.updateRouteVisibilityTo(false);
        }else{
            if(this.waypoints[0].coordinates() == this.destination.coordinates()){
                this.destination = null;
                this.waypoints[0] = null;
                this.getRoute(origin, origin);
            }else {
                if (this.waypoints.length == 2) {
                    this.destination = null;
                    this.destination = this.waypoints[this.waypoints.length - 2];
                    Point[] n = new Point[this.waypoints.length - 1];
                    System.arraycopy(this.waypoints, 0, n, 0, this.waypoints.length - 1);
                    this.waypoints = new Point[n.length];
                    System.arraycopy(n, 0, this.waypoints, 0, n.length);
                    this.getRoute(this.origin,this.destination);
                } else{
                    this.destination = null;
                    this.destination = this.waypoints[this.waypoints.length - 2];
                    Point[] n = new Point[this.waypoints.length - 2];
                    System.arraycopy(this.waypoints, 0, n, 0, this.waypoints.length - 2);
                    this.waypoints = new Point[n.length];
                    System.arraycopy(n, 0, this.waypoints, 0, n.length);
                    this.addRoute(this.destination);
                }
            }
        }
        this.getRouteUser(destination);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_AUTOCOMPLETE) {
            mapboxMap.clear();
            // Retrieve selected location's CarmenFeature
            CarmenFeature selectedCarmenFeature = PlaceAutocomplete.getPlace(data);
            // Create a new FeatureCollection and add a new Feature to it using selectedCarmenFeature above.
            // Then retrieve and update the source designated for showing a selected location's symbol layer icon
            if (mapboxMap != null) {
                Style style = mapboxMap.getStyle();
                if (style != null) {
                    GeoJsonSource source = style.getSourceAs(LINE_GEOJSON_SOURCE_ID);
                    if (source != null) {
                        source.setGeoJson(FeatureCollection.fromFeatures(
                                new Feature[]{Feature.fromJson(selectedCarmenFeature.toJson())}));
                    }
                    if (origin == null) {
                        origin = Point.fromLngLat(((Point) Objects.requireNonNull(selectedCarmenFeature.geometry())).longitude(),
                                ((Point) Objects.requireNonNull(selectedCarmenFeature.geometry())).latitude());
                    } else {
                        destination = Point.fromLngLat(((Point) Objects.requireNonNull(selectedCarmenFeature.geometry())).longitude(),
                                ((Point) Objects.requireNonNull(selectedCarmenFeature.geometry())).latitude());
                    }
                    // Move map camera to the selected location
                    mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(
                            new CameraPosition.Builder()
                                    .target(new LatLng(((Point) Objects.requireNonNull(selectedCarmenFeature.geometry())).latitude(),
                                            ((Point) Objects.requireNonNull(selectedCarmenFeature.geometry())).longitude()))
                                    .zoom(15)
                                    .build()), 4000);
                }
            }
            if (destination == null) {
                this.getRoute(origin, origin);
            } else {
                manageRoutes(destination);
            }
        }
        else if(resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_ADD_HABITUAL_POINT){
            CarmenFeature selectedCarmenFeature = PlaceAutocomplete.getPlace(data);
            Point point = Point.fromLngLat(((Point) Objects.requireNonNull(selectedCarmenFeature.geometry())).longitude(),
                    ((Point) Objects.requireNonNull(selectedCarmenFeature.geometry())).latitude());
            Style style = mapboxMap.getStyle();
            if (style != null) {
                GeoJsonSource source = style.getSourceAs(LINE_GEOJSON_SOURCE_ID);
                if (source != null) {
                    source.setGeoJson(FeatureCollection.fromFeatures(
                            new Feature[]{Feature.fromJson(selectedCarmenFeature.toJson())}));
                }
                // Move map camera to the selected location
                mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(
                        new CameraPosition.Builder()
                                .target(new LatLng(point.latitude(), point.longitude()))
                                .zoom(15)
                                .build()), 4000);
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                String m = this.getApplicationContext().getString(R.string.tile_save_Habitualpoint_dialog);
                builder.setTitle(m);

                // Set up the input
                final EditText input = new EditText(this.getApplicationContext());
                // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                input.setInputType(InputType.TYPE_CLASS_TEXT);
                builder.setView(input);

                // Set up the buttons
                builder.setPositiveButton("OK", (dialog, which) -> {
                    // write object to file
                    try {
                        File file = new File(getApplicationContext().getFilesDir(), FILENAME_HABITUAL_POINTS);
                        StringBuilder temp = new StringBuilder();
                        HabitualPoint pwrite = new HabitualPoint(point, input.getText().toString());
                        HabitualPointList pointList = new HabitualPointList();
                        if (!file.exists()) {
                            file.createNewFile();
                        } else {
                            FileReader filereader = new FileReader(file);
                            BufferedReader br = new BufferedReader(filereader);
                            String sCurrentLine;
                            while ((sCurrentLine = br.readLine()) != null) {
                                temp.append(sCurrentLine);
                            }
                            pointList = HabitualPointList.processStringToArrayListRoutes(temp.toString());
                        }

                        if(pointList.toString().contains(pwrite.toString())) {
                            AlertDialog.Builder errordiag = new AlertDialog.Builder(this);
                            errordiag.setMessage(R.string.errorSave)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                        }
                                    }).show();
                        }else {
                            pointList.add(pwrite);
                            FileWriter fw = new FileWriter(file.getAbsoluteFile());
                            BufferedWriter bw = new BufferedWriter(fw);
                            bw.write(pointList.toString());
                            bw.close();
                            Toast.makeText(this, R.string.habitual_point_added, Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
                builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());
                builder.show();
            }
        }
    }

    @SuppressWarnings({"MissingPermission"})
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
        // Check if permissions are enabled and if not request
        if (PermissionsManager.areLocationPermissionsGranted(this)) {
            // Activate the MapboxMap LocationComponent to show user location
            // Adding in LocationComponentOptions is also an optional parameter
            locationComponent = mapboxMap.getLocationComponent();
            locationComponent.activateLocationComponent(this, loadedMapStyle);
            locationComponent.setLocationComponentEnabled(true);
            // Set the component's camera mode
            locationComponent.setCameraMode(CameraMode.TRACKING);
        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(this);
        }
        if(this.origin != null && this.destination != null) {
            if (this.userRoute == null) {
                userLocation = Point.fromLngLat(locationComponent.getLastKnownLocation().getLongitude(),
                        locationComponent.getLastKnownLocation().getLatitude());
                this.getRouteUser(destination);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.maps_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.add_habitual_point:
                addHabitualPoint();
                return true;
            case R.id.make_route_with_points:
                makeHabitualRouteWithPoints();
                return true;
            case R.id.see_habitual_points:
                seeHabitualPoints();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void addHabitualPoint(){
        this.userLocation = Point.fromLngLat(locationComponent.getLastKnownLocation().getLongitude(),
                locationComponent.getLastKnownLocation().getLatitude());
        Intent intent = new PlaceAutocomplete.IntentBuilder()
                .accessToken(Mapbox.getAccessToken() != null ? Mapbox.getAccessToken() : getString(R.string.mapbox_access_token))
                .placeOptions(PlaceOptions.builder()
                        .backgroundColor(Color.parseColor("#EEEEEE"))
                        .limit(10)
                        .proximity(this.userLocation)
                        .build(PlaceOptions.MODE_CARDS))
                .build(MapsActivity.this);
        startActivityForResult(intent, REQUEST_CODE_ADD_HABITUAL_POINT);
    }

    private void makeHabitualRouteWithPoints(){
        Toast.makeText(this, "MAKE HABITUAL ROUTE", Toast.LENGTH_LONG).show();
        startActivity(new Intent(MapsActivity.this, ListMakeHabitualRoute.class));
        //startActivity(new Intent(MapsActivity.this, TestSpinner.class));
    }

    private void seeHabitualPoints(){
        startActivity(new Intent(MapsActivity.this, ListHabitualPointsActivity.class));
    }

    // Add the mapView lifecycle to the activity's lifecycle methods
    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }


    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mapboxMap != null) {
            mapboxMap.removeOnMapClickListener(this);
        }
        mapView.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(@NotNull Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
        Toast.makeText(this, R.string.user_location_permission_explanation, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            enableLocationComponent(Objects.requireNonNull(mapboxMap.getStyle()));
        } else {
            Toast.makeText(this, R.string.user_location_permission_not_granted, Toast.LENGTH_LONG).show();
            finish();
        }
    }
}
